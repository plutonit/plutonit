# Developer setup

## Prerequisites

- [just](https://github.com/casey/just#installation)
- [podman](https://podman.io/docs/installation)
- [rust](https://rustup.rs/)
- [trunk](https://trunkrs.dev/#install)
- [sqlx-cli](https://github.com/launchbadge/sqlx/blob/main/sqlx-cli/README.md#install)
- [pnpm](https://pnpm.io/installation)

## Backend

Start the database:

```shell
podman compose up
```

Run database migrations:

```shell
just sqlx migrate up
```

Start the api server:

```shell
cargo run
```

If you have [cargo-watch](https://github.com/watchexec/cargo-watch#install) installed, you can also run:

```sh
cargo dev
```

## User interface

Start the trunk development server:

```shell
just trunk serve
```
