use convert_case::{Case, Casing};
use derive_name::Name;
use serde::de::DeserializeOwned;
use serde::Serialize;

pub trait Procedure: Serialize + DeserializeOwned + Name {
    type Response: Serialize + DeserializeOwned;

    fn endpoint_name() -> String {
        <Self as Name>::name().to_case(Case::Kebab)
    }
}

pub fn serialize(procedure: &impl Procedure) -> Result<Vec<u8>, String> {
    rmp_serde::to_vec(procedure).map_err(|e| e.to_string())
}

pub fn deserialize<T: DeserializeOwned>(bytes: &[u8]) -> Result<T, String> {
    rmp_serde::from_slice(bytes).map_err(|e| e.to_string())
}

pub const MSGPACK_MIME_TYPE: &str = "application/vnd.msgpack";
