use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Locale {
    pub id: Uuid,
    pub name: String,
    pub code: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CreateLocaleCommand {
    pub name: String,
    pub code: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct RenameLocaleCommand {
    pub id: Uuid,
    pub name: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct UpdateLocaleCodeCommand {
    pub id: Uuid,
    pub code: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct DeleteLocaleCommand {
    pub id: Uuid,
}
