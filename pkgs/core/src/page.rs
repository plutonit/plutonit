use derive_name::Name;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::api::Procedure;

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct Page {
    pub id: Uuid,
    pub name: String,
    pub path: String,
    pub public: bool,
    pub title: Option<String>,
    pub description: Option<String>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct CreatePageCommand {
    pub name: String,
    pub path: String,
}

impl Procedure for CreatePageCommand {
    type Response = Page;
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct RenamePageCommand {
    pub id: Uuid,
    pub name: String,
}

impl Procedure for RenamePageCommand {
    type Response = ();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct MovePageCommand {
    pub id: Uuid,
    pub locale: Uuid,
    pub path: String,
}

impl Procedure for MovePageCommand {
    type Response = Result<Page, String>;
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct UpdatePageTitleCommand {
    pub id: Uuid,
    pub locale: Uuid,
    pub title: Option<String>,
}

impl Procedure for UpdatePageTitleCommand {
    type Response = ();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct UpdatePageDescriptionCommand {
    pub id: Uuid,
    pub locale: Uuid,
    pub description: Option<String>,
}

impl Procedure for UpdatePageDescriptionCommand {
    type Response = ();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct PublishPageCommand {
    pub id: Uuid,
}

impl Procedure for PublishPageCommand {
    type Response = ();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct UnpublishPageCommand {
    pub id: Uuid,
}

impl Procedure for UnpublishPageCommand {
    type Response = ();
}

#[derive(Clone, Debug, Default, Serialize, Deserialize, Name)]
pub struct DeletePageCommand {
    pub id: Uuid,
}

impl Procedure for DeletePageCommand {
    type Response = ();
}
