use gloo::storage::{LocalStorage, Storage};
use leptos::*;
use leptos_meta::*;
use leptos_router::*;
use tracing::{debug, info};
use tracing_subscriber::fmt::format::Pretty;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_web::{MakeWebConsoleWriter, performance_layer};

use plutonit_core::page::CreatePageCommand;

use crate::api::{run, Token, TOKEN};

mod api;

fn main() {
    console_error_panic_hook::set_once();
    let fmt_layer = tracing_subscriber::fmt::layer()
        .with_ansi(false)
        .without_time()
        .with_writer(MakeWebConsoleWriter::new());
    let perf_layer = performance_layer().with_details_from_fields(Pretty::default());
    tracing_subscriber::registry()
        .with(fmt_layer)
        .with(perf_layer)
        .init();
    mount_to_body(App)
}

#[component]
fn App() -> impl IntoView {
    // let token_store = TokenStore::new().expect("can create token-store");
    // let client = ApiClient::new(token_store).expect("can create api-client");
    // let (get_client, _) = create_signal(client);
    // provide_context(get_client);

    let formatter = |text| format!("{text} - Plutonit");
    provide_meta_context();

    view! {
        <Router>
            <Title formatter />
            <Home />
        </Router>
    }
}

#[component]
fn Home() -> impl IntoView {
    let (count, set_count) = create_signal(0);

    let on_click = move |_| {
        tracing::debug_span!("on_click").in_scope(|| {
            let new_count = count() + 1;
            set_count(new_count);
            let create_page = CreatePageCommand {
                name: "Hello, world!".to_string(),
                path: "/".to_string(),
            };
            debug!("{create_page:?}");
        });
    };

    view! {
        <Title text="Hello, world!" />
        <main>
            <div>
                <img data-trunk src="static/plutonit-figurative.svg" style="height: 4rem" />
                <p>"Hello, world!"</p>
                <p class=("text-brand", move || count() % 2 == 1) >Clicked {count} times</p>
                <button
                    on:click=on_click
                >"click me"</button>
                <br />
            </div>
            <Hello />
        </main>
    }
}

#[component]
fn Hello() -> impl IntoView {
    let (count, set_count) = create_signal(0);
    let (title, set_title) = create_signal(String::new());

    let hello = create_resource(count, move |count| async move {
        let count = count.to_string();
        let create_page = CreatePageCommand {
            name: "Hello".to_string(),
            path: count,
        };

        let pg = run(&create_page).await.unwrap();
        info!("{pg:?}");

        if let Some(meta_title) = pg.title {
            LocalStorage::set(TOKEN, Token(meta_title.clone())).unwrap();
            set_title(meta_title);
        }

        pg.name
    });

    let on_click = move |_| {
        set_count(count() + 1);
    };

    view! {
        <h1>{hello}</h1>
        <pre class="p-2 my-2 font-mono bg-brand">{title}</pre>
        <button on:click=on_click class="p-2 bg-brand rounded">"reload"</button>
    }
}
