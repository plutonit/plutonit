use std::fmt::{Display, Formatter};
use std::ops::Deref;

use gloo::net::http::Request;
use gloo::storage::{LocalStorage, Storage};
use http::header::{ACCEPT, CONTENT_TYPE};
use serde::{Deserialize, Serialize};
use web_sys::js_sys::Uint8Array;

use plutonit_core::api::{deserialize, serialize, Procedure, MIME_TYPE};

pub static TOKEN: &str = "token";

pub async fn run<T: Procedure>(procedure: &T) -> Result<<T as Procedure>::Response, Error> {
    let body = serialize(procedure).map_err(|error| Error::DataFormat(error.to_string()))?;
    let endpoint = T::endpoint_name();
    let mut request = Request::post(&format!("api/{endpoint}"))
        .header(CONTENT_TYPE.as_str(), MIME_TYPE)
        .header(ACCEPT.as_str(), MIME_TYPE);
    if let Ok(token) = LocalStorage::get::<Token>(TOKEN) {
        request = request.header("authorization", &token);
    }
    let request = request
        .body(Uint8Array::from(body.as_slice()))
        .map_err(|error| Error::Unknown(error.to_string()))?;
    let response = request
        .send()
        .await
        .map_err(|error| Error::Network(error.to_string()))?;
    let bytes = response
        .binary()
        .await
        .map_err(|error| Error::Unknown(error.to_string()))?;
    deserialize(bytes.as_slice()).map_err(|error| Error::DataFormat(error.to_string()))
}

#[allow(unused)]
#[derive(Debug)]
pub enum Error {
    DataFormat(String),
    Network(String),
    Authentication(String),
    Permissions(String),
    Unknown(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::DataFormat(error) => error.fmt(f),
            Error::Network(error) => error.fmt(f),
            Error::Authentication(error) => error.fmt(f),
            Error::Permissions(error) => error.fmt(f),
            Error::Unknown(error) => error.fmt(f),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Token(pub String);

// TODO: maybe implement something else

impl Deref for Token {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
