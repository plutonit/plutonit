use std::process::Command;

fn main() {
    println!("cargo::rerun-if-changed=src");
    println!("cargo::rerun-if-changed=index.html");
    println!("cargo::rerun-if-changed=tailwind.input.css");
    println!("cargo::rerun-if-changed=tailwind.config.js");
    println!("cargo::rerun-if-changed=package.json");

    Command::new("pnpm")
        .arg("install")
        .status()
        .expect("run pnpm install");

    Command::new("pnpm")
        .args(["exec", "tailwindcss"])
        .args(["--config", "tailwind.config.js"])
        .args(["--input", "tailwind.input.css"])
        .args(["--output", "assets/styles.min.css"])
        .arg("--minify")
        .status()
        .expect("run tailwind");
}
