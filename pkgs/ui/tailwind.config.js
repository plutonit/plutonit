/** @type {import('tailwindcss').Config} */
module.exports = {
    content: {
        files: ["*.html", "./src/**/*.rs"],
    },
    theme: {
        fontFamily: {
            sans: 'var(--font-sans)',
            mono: 'var(--font-mono)',
        },
        colors: {
            inherit: 'inherit',
            transparent: 'transparent',
            current: 'currentColor',
            white: 'white',
            black: 'black',
            brand: 'var(--brand)',
        },
        textColor: {
            DEFAULT: 'var(--text)',
            soft: 'var(--text-soft)',
            light: 'var(--text-light)',
            brand: 'var(--text-brand)',
        },
        backgroundColor: {
            DEFAULT: 'var(--surface)',
            light: 'var(--surface-light)',
            brand: 'var(--brand)',
        },
        borderColor: {
            DEFAULT: "var(--border)",
            brand: 'var(--brand)',
        },
    },
};
