use std::fmt::{Display, Formatter};

use axum::async_trait;
use axum::body::to_bytes;
use axum::extract::{FromRequest, FromRequestParts, Request};
use axum::http::{HeaderValue, StatusCode};
use axum::http::header::{ACCEPT, CONTENT_TYPE};
use axum::http::request::Parts;
use axum::response::{IntoResponse, Response};
use serde::de::DeserializeOwned;
use serde::Serialize;

use plutonit_core::api::MSGPACK_MIME_TYPE;

use crate::SharedState;

pub struct Data<T>(pub T);

#[async_trait]
impl<T: DeserializeOwned> FromRequest<SharedState> for Data<T> {
    type Rejection = Response;

    async fn from_request(req: Request, state: &SharedState) -> Result<Self, Self::Rejection> {
        let content_type = DataFormat::try_from(req.headers().get(CONTENT_TYPE))
            .map_err(|_| StatusCode::UNSUPPORTED_MEDIA_TYPE.into_response())?;
        let bytes = to_bytes(req.into_body(), state.config.max_payload_size)
            .await
            .map_err(|_| StatusCode::PAYLOAD_TOO_LARGE.into_response())?;
        match content_type.deserializer()(&bytes) {
            Ok(data) => Ok(Data(data)),
            Err(error) => Err((StatusCode::BAD_REQUEST, error).into_response()),
        }
    }
}

impl<T: Serialize> IntoResponse for Data<(T, DataFormat)> {
    fn into_response(self) -> Response {
        let data = self.0 .0;
        let format = self.0 .1;
        let bytes = match format.serializer()(&data) {
            Err(error) => return (StatusCode::INTERNAL_SERVER_ERROR, error).into_response(),
            Ok(bytes) => bytes,
        };
        ([(CONTENT_TYPE, &format.to_string())], bytes).into_response()
    }
}

#[derive(Debug, Default, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub enum DataFormat {
    #[default]
    MessagePack,
    Json,
}

impl Display for DataFormat {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mime = match self {
            DataFormat::MessagePack => MSGPACK_MIME_TYPE,
            DataFormat::Json => JSON_MIME_TYPE,
        };
        write!(f, "{mime}")
    }
}

impl TryFrom<&str> for DataFormat {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            MSGPACK_MIME_TYPE => Ok(Self::MessagePack),
            JSON_MIME_TYPE => Ok(Self::Json),
            _ => Err(()),
        }
    }
}

impl TryFrom<&HeaderValue> for DataFormat {
    type Error = ();

    fn try_from(header: &HeaderValue) -> Result<Self, Self::Error> {
        let value = header.to_str().map_err(|_| ())?;
        Self::try_from(value)
    }
}

impl TryFrom<Option<&HeaderValue>> for DataFormat {
    type Error = ();

    fn try_from(value: Option<&HeaderValue>) -> Result<Self, Self::Error> {
        let value = value.ok_or(())?;
        Self::try_from(value)
    }
}

#[async_trait]
impl<S> FromRequestParts<S> for DataFormat {
    type Rejection = ();

    async fn from_request_parts(parts: &mut Parts, _: &S) -> Result<Self, Self::Rejection> {
        Ok(DataFormat::try_from(parts.headers.get(ACCEPT)).unwrap_or_default())
    }
}

impl DataFormat {
    pub fn deserializer<T: DeserializeOwned>(&self) -> fn(bytes: &[u8]) -> Result<T, String> {
        match self {
            DataFormat::MessagePack => deserialize_msgpack,
            DataFormat::Json => deserialize_json,
        }
    }

    pub fn serializer<T: Serialize>(&self) -> fn(data: &T) -> Result<Vec<u8>, String> {
        dbg!(self);
        match self {
            DataFormat::MessagePack => serialize_msgpack,
            DataFormat::Json => serialize_json,
        }
    }
}

pub fn deserialize_msgpack<T: DeserializeOwned>(bytes: &[u8]) -> Result<T, String> {
    rmp_serde::from_slice(bytes).map_err(|e| e.to_string())
}

pub fn deserialize_json<T: DeserializeOwned>(bytes: &[u8]) -> Result<T, String> {
    serde_json::from_slice(bytes).map_err(|e| e.to_string())
}

pub fn serialize_msgpack<T: Serialize>(data: &T) -> Result<Vec<u8>, String> {
    rmp_serde::to_vec(data).map_err(|e| e.to_string())
}

pub fn serialize_json<T: Serialize>(data: &T) -> Result<Vec<u8>, String> {
    serde_json::to_vec(data).map_err(|e| e.to_string())
}
