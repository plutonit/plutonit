use axum::extract::State;
use axum::http::header::AUTHORIZATION;
use axum::http::HeaderMap;
use axum::response::IntoResponse;
use chrono::Utc;
use tracing::debug;
use uuid::Uuid;

use plutonit_core::page::{CreatePageCommand, Page};

use crate::data::{Data, DataFormat};
use crate::SharedState;

pub async fn create_page_command_handler(
    State(_): State<SharedState>,
    headers: HeaderMap,
    data_format: DataFormat,
    Data(command): Data<CreatePageCommand>,
) -> impl IntoResponse {
    debug!("{command:?}");
    let token = headers
        .get(AUTHORIZATION)
        .map(|h| h.to_str().unwrap())
        .unwrap_or_default();
    debug!("{token}");
    let page = Page {
        id: Uuid::new_v4(),
        name: command.name,
        path: command.path,
        public: false,
        title: Some(Utc::now().to_string()),
        description: None,
    };
    Data((page, data_format))
}
