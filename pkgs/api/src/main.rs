use std::env::current_dir;
use std::net::{IpAddr, SocketAddr};
use std::path::PathBuf;
use std::str::FromStr;
use std::time::{Duration, Instant};

use axum::extract::Request;
use axum::http::header::{CACHE_CONTROL, SERVER};
use axum::http::HeaderValue;
use axum::middleware::{from_fn, Next};
use axum::response::Response;
use axum::Router;
use axum::routing::{get, post};
use config::{Config, Environment};
use gethostname::gethostname;
use humantime::format_duration;
use serde::Deserialize;
use sqlx::ConnectOptions;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use tokio::{join, signal, spawn};
use tokio::net::TcpListener;
use tokio::task::JoinHandle;
use tokio_util::sync::CancellationToken;
use tower::ServiceBuilder;
use tower_http::compression::CompressionLayer;
use tower_http::normalize_path::NormalizePathLayer;
use tower_http::services::ServeDir;
use tower_http::set_header::SetResponseHeaderLayer;
use tower_http::trace::TraceLayer;
use tracing::info;
use tracing_subscriber::{fmt, Layer, registry};
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use url::Url;

use plutonit_core::api::Procedure;
use plutonit_core::page::CreatePageCommand;

use crate::page::create_page_command_handler;

mod data;
mod page;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let config = Configuration::new()?;
    let token = CancellationToken::new();

    let tracing_handle = setup_tracing(&config, token.clone())?;
    info!("Hello");

    let server_handle = start_server(config.try_into()?, token.clone()).await?;

    shutdown_signal().await;
    token.cancel();
    let (_, _) = join!(tracing_handle, server_handle);

    info!("Bye");
    Ok(())
}

pub async fn start_server(
    state: SharedState,
    token: CancellationToken,
) -> anyhow::Result<JoinHandle<()>> {
    let server_value = HeaderValue::from_static("Axum (Hyper)");
    let server_header = SetResponseHeaderLayer::overriding(SERVER, server_value);
    let service_builder = ServiceBuilder::new()
        .layer(from_fn(response_time))
        .layer(TraceLayer::new_for_http())
        .layer(server_header)
        .layer(CompressionLayer::new())
        .layer(NormalizePathLayer::trim_trailing_slash());

    let addr = SocketAddr::new(state.config.bind_addr, state.config.bind_port);
    info!("listening on http://{addr}");
    let listener = TcpListener::bind(addr).await?;

    let make_service = Router::new()
        .merge(static_files(&state.config)?)
        .merge(api_routes())
        .layer(service_builder)
        .with_state(state);

    Ok(spawn(async move {
        axum::serve(listener, make_service)
            .with_graceful_shutdown(token.cancelled_owned())
            .await
            .unwrap();
    }))
}

pub fn api_routes() -> Router<SharedState> {
    Router::new()
        .route("/api/hello", get(|| async { "Hello, world!" }))
        .route(
            &path::<CreatePageCommand>(),
            post(create_page_command_handler),
        )
    // TODO: implement routes here
}

fn path<P: Procedure>() -> String {
    format!("/api/{}", P::endpoint_name())
}

async fn response_time(request: Request, next: Next) -> Response {
    let start = Instant::now();
    let mut response = next.run(request).await;
    let duration = start.elapsed();
    if let Ok(value) = HeaderValue::from_str(&format_duration(duration).to_string()) {
        response.headers_mut().insert("x-response-time", value);
    }
    response
}

fn static_files(config: &Configuration) -> anyhow::Result<Router<SharedState>> {
    let static_dir = current_dir()?.join(&config.static_dir);
    let cache_one_day = HeaderValue::from_static("public, stale-while-revalidate, max-age=86400");
    let cache_control = SetResponseHeaderLayer::overriding(CACHE_CONTROL, cache_one_day);
    let router = Router::new()
        .nest_service("/", ServeDir::new(&static_dir))
        .layer(cache_control);

    let static_dir_str = static_dir.to_string_lossy();
    info!("serving assets files from {static_dir_str}");

    Ok(router)
}

#[derive(Clone, Debug, Deserialize)]
pub struct Configuration {
    pub bind_addr: IpAddr,
    pub bind_port: u16,
    pub static_dir: PathBuf,
    pub database_url: String,
    pub loki_url: Option<Url>,
    pub max_payload_size: usize,
}

pub static BYTES_IN_MEBIBYTE: usize = 1048576;

impl Configuration {
    pub fn new() -> anyhow::Result<Self> {
        let config = Config::builder()
            .set_default("bind_addr", "0.0.0.0")?
            .set_default("bind_port", "7442")?
            .set_default("static_dir", "dist")?
            .set_default("database_url", "plutonit:plutonit@localhost/plutonit")?
            .set_default("max_payload_size", BYTES_IN_MEBIBYTE.to_string())?
            .add_source(Environment::with_prefix("PLUTONIT"))
            .build()?
            .try_deserialize()?;
        Ok(config)
    }
}

fn setup_tracing(
    config: &Configuration,
    token: CancellationToken,
) -> anyhow::Result<JoinHandle<()>> {
    if let Some(url) = &config.loki_url {
        let host = gethostname().into_string().unwrap_or_default();
        let (layer, controller, task) = tracing_loki::builder()
            .extra_field("host", host)?
            .build_controller_url(url.to_owned())?;

        registry()
            .with(fmt::layer().with_filter(LevelFilter::DEBUG))
            .with(layer.with_filter(LevelFilter::INFO))
            .init();

        spawn(task);

        Ok(spawn(async move {
            token.cancelled().await;
            controller.shutdown().await;
        }))
    } else {
        fmt::fmt().with_max_level(LevelFilter::DEBUG).init();
        Ok(spawn(token.cancelled_owned()))
    }
}

#[derive(Clone, Debug)]
pub struct SharedState {
    pub pool: sqlx::PgPool,
    pub config: Configuration,
}

impl TryFrom<Configuration> for SharedState {
    type Error = sqlx::Error;

    fn try_from(config: Configuration) -> Result<Self, Self::Error> {
        let connect_options =
            PgConnectOptions::from_str(&config.database_url)?.disable_statement_logging();
        let pool = PgPoolOptions::default()
            .max_connections(8)
            .acquire_timeout(Duration::from_secs(5))
            .connect_lazy_with(connect_options);
        Ok(SharedState { pool, config })
    }
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => info!("received SIGINT"),
        _ = terminate => info!("received SIGTERM"),
    }
}
