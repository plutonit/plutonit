# Plutonit

Plutonit *(plutoˈniːt)* is a rock solid headless cms.

## License

<small>Copyright (C) 2024 Abineo AG</small>  
<small>This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
later version.</small>  
<small>The name "Plutonit" is a registered trademark of Abineo AG, and Abineo AG hereby declines to grant a trademark
license to "Plutonit" pursuant to the GNU Affero General Public License version 3 Section 7(e), without a separate
agreement with Abineo AG.</small>  
<small>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
more details.</small>  
<small>You should have received a copy of the GNU Affero General Public License along with this program. If not,
see <https://www.gnu.org/licenses/>.</small>

## Developer setup

See [docs/dev-setup.md](docs/dev-setup.md).
